from stack1 import Stack

def rev_str(stack, input_str):

	str_rev = ""
	for i in range(len(input_str)):
		stack.push(input_str[i])

	while not stack.is_empty():
		str_rev += stack.pop()
	return str_rev

stack = Stack()
input_str = 'Hello'
print(rev_str(stack, input_str))





