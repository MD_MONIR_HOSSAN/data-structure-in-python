"""
	Stack data structure
	Push
	A
	B
	C
	D
	Pop
	[A, B, C, D]
"""

class Stack():

	def __init__(self):
		self.items = []

	def is_empty(self):
		return self.items == []

	def push(self, item):
		return self.items.append(item)

	def pop(self):
		return self.items.pop()

	def peek(self):
		if not self.is_empty():
			return self.items[-1]

	def  get_stack(self):
		return self.items

# s = Stack()
# item_list = [1, 2, 3, 4, 5, 6]
# print(s.is_empty())
# print(s.peek())
# for i in item_list:
# 	s.push(i)
# print('After pushing item_list : ')
# print(s.get_stack())
# print('Top item : ')
# print(s.peek())
# s.push('A')
# print('After pushing A : ')
# print(s.get_stack())
# print(s.peek())
# print('Pop 1 item : ')
# s.pop()
# print(""" Top item """)
# print(s.peek())
# print(s.get_stack())
# print('Is stack is empty : ')
# print(f"""  {s.is_empty()} """)





