def fizz_buzz(num):
	for i in range(1, num + 1):
		if i % 3 == 0 and i % 5 == 0:
			print(f' {i} is Fizz Buzz')
		elif i % 3 == 0 :
			print(f' {i} is Fizz')
		elif i % 5 == 0:
			print(f' {i} is Buzz')
		else:
			print(f' {i} is Not a Fizz Buzz')

fizz_buzz(30)