import time
#     0    15   0    15   0    25   0   60   30
A = [310, 315, 275, 290, 265, 290, 240, 300, 270]

# Time complexity O(n^2)
# Space complexity O(1)
t1 = time.time()
def buy_sell_once(A):
	max_profit = 0
	for i in range(len(A) - 1):
		for j in range(i + 1, len(A)):
			if A[j] - A[i] > max_profit:
				max_profit = A[j] - A[i]
	return max_profit


print(buy_sell_once(A))
t2 = time.time()
print(t2 - t1)

# Time complexity O(n)
# Space complexity O(1)
def buy_sell_once2(A):
	min_price = A[0]
	max_profit = 0

	for price in A:
		min_price = min(price, min_price)

		compare_profit = price - min_price

		max_profit = max(max_profit, compare_profit)
	return max_profit

print(buy_sell_once2(A))
