import time
# Two sum Problem
A = [-2, 1, 3, 6, 6, 3, 20]
target = 9

# Time complexity O(n^2)
# Space complexity O(1)
def two_sum_broute_force(A, target):
	for i in range(len(A)):
		for j in range(i+1, len(A)):
			if A[i] + A[j] == target:
				print(A[i], A[j])
				return True
	return False

t1 = time.time()
print(two_sum_broute_force(A, target))
t2 = time.time()
print(t2 - t1)


# Time complexity O(n)
# Space complexity O(n)
def two_sum_hastable(A, target):
	ht = dict()
	for i in range(len(A)):
		if A[i] in ht:
			print(ht[A[i]], A[i])
			print(ht[A[i]] + A[i])
			return True
		else:
			ht[target - A[i]] = A[i]
	return False

t3 = time.time()
print(two_sum_hastable(A, target))
t4 = time.time()
print(t4 - t3)


# Time complexity O(n)
# Space complexity O(1)
def two_sum(A, target):
	i = 0
	j = len(A) - 1

	while i <= j:
		if A[i] + A[j] == target:
			print(A[i], A[j])
			return True
		elif A[i] + A[j] < target:
			i += 1
		else:
			j -= 1
	return False


t5 = time.time()
print(two_sum(A, target))
t6 = time.time()
print(t6 - t5)



