"""
	Singly Link List Insertion
	Append to last: D
		A->B->C->D->Null
	Append to First: E
		E->A->B->C->D->Null
	Append between node: F
		E->A->B->F->C->D->Null
"""
import time
class Node:
	def __init__(self, data):
		self.data = data
		self.next = None

class LinkList:
	def __init__(self):
		self.head = None

	def print_list(self):
		cur_node = self.head
		while cur_node:
			print(cur_node.data)
			cur_node = cur_node.next

	def append(self, data):
		new_node = Node(data)

		if self.head is None:
			self.head = new_node
			return

		last_node = self.head
		while last_node.next:
			last_node = last_node.next
		last_node.next = new_node

	def prepend(self, data):
		new_node = Node(data)

		if self.head is None:
			self.head = new_node
		new_node.next = self.head
		self.head = new_node

	def insert_after_node(self, prev_node, data):
		new_node = Node(data)

		if not prev_node:
			print('Previous node is not in the list')
			return
		new_node.next = prev_node.next
		prev_node.next = new_node

	def delete_node(self, key):
		cur_node = self.head
		prev_node = None

		if cur_node and cur_node.data == key:
			self.head = cur_node.next
			cur_node = None
			return

		while cur_node and cur_node.data != key:
			prev_node = cur_node
			cur_node = cur_node.next

		if cur_node is None:
			return
		prev_node.next = cur_node.next
		cur_node = None

	def delete_node_in_position(self, pos):
		cur_node = self.head
		prev_node = None
		count = 0

		if pos == 0:
			self.head = cur_node.next
			cur_node = None
			return

		while cur_node and count != pos:
			prev_node = cur_node
			cur_node = cur_node.next
			count += 1

		if cur_node is None:
			return
		prev_node.next = cur_node.next
		cur_node = None


	def list_length(self):
		cur_node = self.head

		count = 0

		if cur_node is None:
			return

		while cur_node:
			cur_node = cur_node.next

			count += 1

		return count

	def list_length_recursively(self, node):
		if node is None:
			return 0
		return 1 + self.list_length_recursively(node.next)


	def swap_node(self, key_1, key_2):
		
		if key_1 == key_2:
			return

		prev_1 = None
		cur_1 = self.head
		while cur_1 and cur_1.data != key_1:
			prev_1 = cur_1
			cur_1 = cur_1.next

		print(prev_1.data)
		print(cur_1.data)
		prev_2 = None
		cur_2 = self.head
		while cur_2 and cur_2.data != key_2:
			prev_2 = cur_2
			cur_2 = cur_2.next
		print(prev_2.data)
		print(cur_2.data)

		if not cur_1 or not cur_2:
			return

		if prev_1:
			prev_1.next = cur_2

		if prev_2:
			prev_2.next = cur_1

		cur_1.next , cur_2.next = cur_2.next, cur_1.next

t0 = time.time()
l_list = LinkList()
l_list.append('A')
l_list.append('B')
l_list.append('C')
l_list.append('D')
# l_list.prepend('E')
l_list.swap_node('B', 'C')
l_list.insert_after_node(l_list.head.next, 'F')
l_list.delete_node_in_position(2)
l_list.print_list()
# print(l_list.head.next.data)
print(f"List length : {l_list.list_length()}")

print(f"List length with recursive : {l_list.list_length_recursively(l_list.head)}")
# for n in range(1, 100):
# 	l_list.prepend(n)
# print(l_list.print_list())
t1 = time.time()

print(t1 - t0)





